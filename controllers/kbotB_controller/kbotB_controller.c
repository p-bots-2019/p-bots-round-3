#include <webots/robot.h>
#include <webots/nodes.h>
#include <webots/supervisor.h>
#include <webots/camera.h>
#include <webots/range_finder.h>

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h> 

#include <arm.h>
#include <base.h>
#include <gripper.h>

#define TIME_STEP 16
#define KBOT_INDEX 1

int main(int argc, char **argv) {
  // useless
}
