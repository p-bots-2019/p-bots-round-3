#include <webots/robot.h>
#include <webots/nodes.h>
#include <webots/supervisor.h>
#include <webots/camera.h>
#include <webots/range_finder.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TIME_STEP 16
#define NO_BOXES 30

// For identification of boxes
enum BoxColor { Red = 0, Green = 1, Blue = 2 };

struct Box
{
  double x,y;
  enum BoxColor color;
};

struct Box boxes[30];

// range finder properties
int rf_width, rf_height;
float distance;

// camera properties
int width, height;
int red, blue, green;  
  
// initialization: kinect
WbDeviceTag camera;
WbDeviceTag range_finder;
WbDeviceTag emitter;
WbDeviceTag receiver;

FILE *fp;

// KKB
int i, j;
WbNodeRef KKboxes[NO_BOXES];
char Obs_names[NO_BOXES][6];
char tmp[3];
double values[NO_BOXES][3]; 

FILE *fp;
int count = 0;

WbNodeRef KBotR;
WbNodeRef KBotB;
const double *RPosition;
const double *BPosition;
char command1[25] = "0";
char command2[25] = "-1";

double graceLength = 0.02;

int number_of_objects = 0;

struct Box nearestRedBox;
struct Box nearestBlueBox;

double cylinderHeight = 0;

void initialization()
{
  // Initialize the kinect albedo camera  
  camera = wb_robot_get_device("kinect color");
  wb_camera_enable(camera, TIME_STEP);
  wb_camera_recognition_enable(camera, TIME_STEP);
  
  width = wb_camera_get_width(camera);
  height = wb_camera_get_height(camera);
  
  // Initialize the kinect depth sensors
  range_finder = wb_robot_get_device("kinect range");
  wb_range_finder_enable(range_finder, TIME_STEP);
  
  rf_width = wb_range_finder_get_width(range_finder);
  rf_height = wb_range_finder_get_height(range_finder);
  
  // Initialize emitter and receiver
  // emitter = wb_robot_get_device("kinect_emitter");
  // receiver = wb_robot_get_device("receiver");
}

void placeBoxesRandomly()
{
  // initialization: color-boxes
  
  /* random position vectors-values- for color boxes defined as KKBx */
  srand(time(0));  
  for(i = 0; i<NO_BOXES; i++){
    char kb[6]={"KKB"};
    sprintf(tmp, "%d", i); 
    strcpy(Obs_names[i], strcat(kb, tmp));   
    for(j = 0; j<3; j++){ 
      values[i][j]= (rand()%95-47)/10.0;      
      //values[i][j]= (rand()%81-35)/10;
      values[i][1]= 0.04;
      //fprintf(stderr, "VALUE, %d\n",values[i][1] );
    }
    if (fabs(values[i][0])<1.5 && fabs(values[i][2])<1.5){
      if (values[i][0]<0){
        values[i][0]= values[i][0]-1.5;
      }
      else{
        values[i][0]= values[i][0]+1.5;
      }
    }
  }
  /* setting KKBx translations to above random values */
  for (i = 0; i < NO_BOXES ; i++) {
    KKboxes[i] = wb_supervisor_node_get_from_def(Obs_names[i]);
    WbFieldRef tr = wb_supervisor_node_get_field(KKboxes[i], "translation");
    wb_supervisor_field_set_sf_vec3f(tr, values[i]);
  }
}

void setCylinderHeight()
{
  // random cylindrical tower platform height
  WbNodeRef TowerSD, TowerSH;
  float rvalue;
  double TWtl[1][3];
  rvalue = (rand() % 36 + 20) / 100.0;
  TWtl[0][0]=1;
  TWtl[0][1]=0.001+rvalue/2;
  TWtl[0][2]=0;
  TowerSD = wb_supervisor_node_get_from_def("TOWER");  
  WbFieldRef hy = wb_supervisor_node_get_field(TowerSD, "translation");
  TowerSH = wb_supervisor_node_get_from_def("TOWER_cy");  
  WbFieldRef ht = wb_supervisor_node_get_field(TowerSH, "height");
  wb_supervisor_field_set_sf_vec3f(hy, TWtl[0]);
  wb_supervisor_field_set_sf_float(ht, rvalue);    
  
  printf("Set the cylinder height to: %f\n", rvalue);
  
  cylinderHeight = rvalue;
}

void getCylinderHeight()
{
  const float *rangeImage = wb_range_finder_get_range_image(range_finder);
  printf("%f", wb_range_finder_image_get_depth(rangeImage, rf_width, (rf_width /2), (rf_height / 2)));
  printf("max range %f\n", wb_range_finder_get_min_range(range_finder));
}

// Get the position of the robots
void getRobotPositions()
{
  // Get position for the objects
  RPosition = wb_supervisor_node_get_position(KBotR);
  BPosition = wb_supervisor_node_get_position(KBotB);
}

// Determine the color of the objects list
enum BoxColor determineColor(const WbCameraRecognitionObject *objects, int i)
{
  if (objects[i].colors[0] > (objects[i].colors[1] + objects[i].colors[2]))
    return Red;
  else if (objects[i].colors[1] > (objects[i].colors[0] + objects[i].colors[2]))
    return Green;
  else 
    return Blue;
}

// Gets the positions of all recognized boxes in the plane and convert the image positions to global positions
void getBoxes()
{
  number_of_objects = wb_camera_recognition_get_number_of_objects(camera);
  const WbCameraRecognitionObject *objects = wb_camera_recognition_get_objects(camera);
  
  for (int k = 0; k < number_of_objects; k++)
  {
    struct Box box;
        
    boxes[k] = box;
        
    boxes[k].color = determineColor(objects, k);      
    boxes[k].x = (((double)objects[k].position_on_image[0] / width) * 10) - 5;
    boxes[k].y = (((double)objects[k].position_on_image[1] / height) * 10) - 5;     
  }
}

void findNearestBox()
{
  // if ((nearestRedBox.x == 0 && nearestRedBox.y == 0))
  if (true)
  {
    // find nearest red box
    double nearestRedDist = 1000;
    double nearestBlueDist = 1000;
    
    for (int e = 0; e < number_of_objects; e++)
    {
      struct Box box = boxes[e];
          
      // don't include those which are already placed
      if ((box.x > 0.5 || box.x < -0.5) && (box.y > 0.5 || box.y < -0.5)
          && (box.x > ) )
      {
        // outside of box
        if (boxes[e].color == 0)
        {
          double dist = sqrt(pow((box.x - RPosition[0]), 2) + pow((box.y - RPosition[2]), 2));
          
          if (dist < nearestRedDist)
          {
            nearestRedDist = dist;
            nearestRedBox = boxes[e];
          }
        }
        else if (boxes[e].color == 2)
        {
          double dist = sqrt(pow((box.x - BPosition[0]), 2) + pow((box.y - BPosition[2]), 2));
          
          if (dist < nearestBlueDist)
          {
            nearestBlueDist = dist;
            nearestBlueBox = boxes[e];
          }
        }
      }
      
    }
  } 
  
  sprintf(command1, "%.3f,%.3f", nearestRedBox.x, nearestRedBox.y);
  sprintf(command2, "%.3f,%.3f", nearestBlueBox.x, nearestBlueBox.y);
}

// write every few seconds
void transmitData()
{
  fp = fopen("../coordinates.txt", "w");
  fprintf(fp, "%s,%s,%f\n", command1, command2, cylinderHeight);
  fclose(fp);
}

void loop()
{
  if (count % 100 == 0)
  {
    getBoxes(); // get the positions of all the boxes
    getRobotPositions(); // get the position of the robots themselves
    findNearestBox(); // Red robot
    transmitData(); // transmit the data to the robots
  }
  count++;
}

int main(int argc, char **argv) {
  // initialization: robot
  wb_robot_init();
   
  initialization();
  placeBoxesRandomly();
  setCylinderHeight();
  getCylinderHeight();
  
  // Get defined object
  KBotR = wb_supervisor_node_get_from_def("KBOTR");
  KBotB =  wb_supervisor_node_get_from_def("KBOTB");
  
  // Main loop
  while (wb_robot_step(TIME_STEP) != -1) {
    loop();
  }
    
  wb_robot_cleanup();
  return 0;
}
