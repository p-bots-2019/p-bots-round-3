#include <webots/robot.h>
#include <webots/nodes.h>
#include <webots/supervisor.h>
#include <webots/distance_sensor.h>
#include <webots/receiver.h>
#include <webots/emitter.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <arm.h>
#include <base.h>
#include <gripper.h>

#define TIME_STEP 16

int KBOT_INDEX = 0;

enum RobotStatus { ReachingBox = 0, PickingUpBox = 1, DeliveringBox = 2, Idle = 3 };

enum RobotStatus MyStatus;

double dropPoints[16][2] = 
{
  {-0.17, -0.17}, {-0.058, -0.17}, {0.058, -0.17}, {0.17, -0.17},
  {-0.17, -0.058}, {-0.058, -0.058}, {0.058, -0.058}, {0.17, -0.058},
  {-0.17, 0.058}, {-0.058, 0.058}, {0.058, 0.058}, {0.17, 0.058},
  {-0.17, 0.17}, {-0.058, 0.17}, {0.058, 0.17}, {0.17, 0.17},
};

double dropPointSelection[2][4] = 
{
  // {0, 3, 12, 15}, {5, 6, 9, 10}  
  {0, 3, 12, 15}, {9, 10, 5, 6}
};

double selectedDropPoints[4][2];
int finishedDropPoints = 0;

WbDeviceTag rec, receiver, emitter;

char buff[255];
FILE *fp;

double targetPosition[2];
double robotPosition[2];

double readPositions[5];

WbNodeRef me;

int i, j;
char *p;
char *q;
bool has_target = false;

int counter = 0;
int file_counter = 0;
double d = 0.425;
double PI = 3.141592;

int time_for_grip_start = 300;
int time_for_grip_end = 150;
int time_for_gripper_positioning = 360;
int time_for_gripper_center_positioning = 450;

WbDeviceTag laser1, laser2, laser3, laser_left, laser_left2, laser_right, laser_right2;
double laser1_dist, laser2_dist, laser3_dist, laser_left1_dist, laser_left2_dist, laser_right1_dist, laser_right2_dist;

double diffX, diffY, targetX, targetY, angle;

double laser_offset_distance_min = 680;
double laser_offset_distance_max = 790;

int arm_ik_counter = 0;

bool further_adjustments = false;

int blocked_counter1 = 0;
double previousPos[2];

bool reversePos = false;

double offset_corner = 0.37;

int ongoingTask = 2;

double cylinderHeight = 0;

void getPosition()
{
  robotPosition[0] = wb_supervisor_node_get_position(me)[0];
  robotPosition[1] = wb_supervisor_node_get_position(me)[2];
}

void readFile()
{
  fp = fopen("../coordinates.txt", "r");
  fscanf(fp, "%s", buff);
  fclose(fp);
  
  i = 0;
  
  p = strtok(buff, ",");
  while (p != NULL)
  {      
    readPositions[i] = atof(p);
    
    // proceed to next split
    p = strtok (NULL, ",");
    i++;
  }
  
  if (KBOT_INDEX == 0)
  {
    targetPosition[0] = readPositions[0];
    targetPosition[1] = readPositions[1];
  } else {
    targetPosition[0] = readPositions[2];
    targetPosition[1] = readPositions[3];
  }
  
  cylinderHeight = readPositions[4]; // Offset for the center point of robot  
}

void selectDropPoints()
{
  for (int i = 0; i < 4; i++)
  {
    int index = dropPointSelection[KBOT_INDEX][i];
    selectedDropPoints[i][0] = dropPoints[index][0];
    selectedDropPoints[i][1] = dropPoints[index][1];
  }
}

void avoidCollision()
{
    // laser_left1_dist 
    // laser_left2_dist 
    // laser_right1_dist 
    // laser_right2_dist
    
    if (laser_left1_dist + laser_left2_dist < 2000)
    {
      base_strafe_right();
      if (targetY > robotPosition[1])
      {
        if (angle == PI / 2)
          base_backwards();
        else 
          base_forwards();
      } 
      else if (targetY < robotPosition[1])
      {
        if(angle == PI / 2)
          base_forwards();
        else if (angle == - PI / 2)
          base_backwards();
      }
    } 
    
    if (laser_right1_dist + laser_right2_dist < 2000)
    {
      base_strafe_left();
      
      if (targetY > robotPosition[1])
      {
        if (angle == PI / 2)
          base_backwards();
        else 
          base_forwards();
      } 
      else if (targetY < robotPosition[1])
      {
        if(angle == PI / 2)
          base_forwards();
        else if (angle == - PI / 2)
          base_backwards();
      }
    }
}

char message[128];
double signal;

int main(int argc, char **argv) {
  // initialization: robot
  wb_robot_init();
  
  printf("%s\n", wb_robot_get_name());
  
  if (strcmp(wb_robot_get_name(), "KBOTR") == 0)
  {
    KBOT_INDEX = 0;
    printf("I'm RED\n");
  } else {
    KBOT_INDEX = 1;
    printf("I'm BLUE\n");
  }
  
  // initialization: robot base, arm and the gripper
  base_init();
  arm_init();
  gripper_init();
  base_goto_init(TIME_STEP);
  me = wb_supervisor_node_get_self();
  
  selectDropPoints();
  
  laser1 = wb_robot_get_device("ds1");
  laser2 = wb_robot_get_device("ds2");
  laser3 = wb_robot_get_device("ds3");
  laser_left = wb_robot_get_device("left_sensor_1");
  laser_right = wb_robot_get_device("right_sensor_1");
  laser_left2 = wb_robot_get_device("left_sensor_2");
  laser_right2 = wb_robot_get_device("right_sensor_2");
  emitter = wb_robot_get_device("emitter");
  receiver = wb_robot_get_device("receiver");
  wb_receiver_enable(receiver, TIME_STEP);
  
  wb_distance_sensor_enable(laser1, TIME_STEP);
  wb_distance_sensor_enable(laser2, TIME_STEP);
  wb_distance_sensor_enable(laser3, TIME_STEP);
  wb_distance_sensor_enable(laser_left, TIME_STEP);
  wb_distance_sensor_enable(laser_right, TIME_STEP);
  wb_distance_sensor_enable(laser_left2, TIME_STEP);
  wb_distance_sensor_enable(laser_right2, TIME_STEP);
  
  // Main loop
  while (wb_robot_step(TIME_STEP) != -1) 
  {    
    laser1_dist = wb_distance_sensor_get_value(laser1);
    laser2_dist = wb_distance_sensor_get_value(laser2);
    laser3_dist = wb_distance_sensor_get_value(laser3);
    laser_left1_dist = wb_distance_sensor_get_value(laser_left);
    laser_left2_dist = wb_distance_sensor_get_value(laser_left2);
    laser_right1_dist = wb_distance_sensor_get_value(laser_right);
    laser_right2_dist = wb_distance_sensor_get_value(laser_right2);
    
    counter++;
    file_counter++;
    blocked_counter1++;
    
    if (file_counter % 100 == 0)
      readFile();
      
    getPosition();
    
    // Send my location
    wb_emitter_send(emitter, robotPosition, 2 * sizeof(double));
    
    // receive other location
    while (wb_receiver_get_queue_length(receiver) > 0) {
      signal = wb_receiver_get_signal_strength(receiver);
      wb_receiver_next_packet(receiver);
    }
    
    base_goto_run();
    
    targetX = targetPosition[0];
    targetY = targetPosition[1];
    
    diffX = pow(targetPosition[0] - robotPosition[0], 2);
    diffY = pow(targetPosition[1] - robotPosition[1], 2);
    
    angle = 0;
    
    if (diffX > diffY)
    {
      if (targetPosition[0] > robotPosition[0])
      {
        // Right side
        angle = 0;
        targetX = targetX - d;
      } else {
        // Left side
        targetX = targetX + d;
        angle = PI;
      }
    } else {
      if (targetPosition[1] > robotPosition[1])
      {
        // Down
        targetY = targetY - d;
        angle = -(PI / 2);
      } else {
        // Up
        targetY = targetY + d;
        angle = PI / 2;
      }
    }
    
    // printf("%f,%f\n", laser1_dist, laser2_dist);
    
    if (MyStatus == PickingUpBox)
    {
      
      if ((KBOT_INDEX == 0) || (KBOT_INDEX == 1 && signal < 2))
      {
        if (counter > time_for_grip_start)
        {
          if(laser3_dist < 1000)
          {
            if (laser3_dist >= laser_offset_distance_min && laser3_dist <= laser_offset_distance_max)
            {              
              base_reset();
              
              if (further_adjustments)
              {
                if (laser1_dist < 1000)
                {
                  printf("Should go left\n");
                  base_strafe_left();
                }
                else if (laser2_dist < 1000)
                {
                  printf("Should go right\n");
                  base_strafe_right();
                }
                else 
                {
                  arm_set_height(ARM_FRONT_FLOOR);
                  
                  if (counter > time_for_grip_start + time_for_grip_end)
                  {                            
                    printf("Must grab the cube\n");
                    gripper_grip();
                    further_adjustments = false;
                  }  
                }
              } else {
                arm_ik_counter = 0;
                MyStatus = DeliveringBox;
                counter = 0;
              }
            }
            else 
            {
              if (laser3_dist > laser_offset_distance_max)
              {
                printf("Too far \n");
                base_forwards();
              } 
              else if (laser3_dist < laser_offset_distance_min)
              {
                printf("Too near \n");
                base_backwards();
              }
            }
          } else {
            if (laser2_dist < 1000)
            {
              base_strafe_right();
            } else if (laser1_dist < 1000)
            {
              base_strafe_left();
            }
            else 
            {
              printf("Couldn't find the box\n");
              base_forwards();
            }
          }
          
        } else {
          arm_set_height(ARM_FRONT_PLATE);
          arm_set_orientation(ARM_FRONT);
        }
      }
    } else if (MyStatus == ReachingBox) {   
        
      if (reversePos)
      {
        if (robotPosition[1] > 0)
        {
          targetY = 1;
          targetX = targetX;
        } else {
          targetY = -1;
          targetX = targetX;
        }
      }
      
      if (laser3_dist < 1000)
      {
        base_strafe_right();
      }
      else 
      {
        if (blocked_counter1 % 500 == 0)
        {
          double d = sqrt(pow(previousPos[0] - robotPosition[0], 2) + pow(previousPos[1] - robotPosition[1], 2));
          
          printf("%f\n",d);
          
          if (d < 0.05)
          {
            // officially stuck
            reversePos = true;
          } else {
            reversePos = false;
          }
          
          previousPos[0] = robotPosition[0];
          previousPos[1] = robotPosition[1];
        }         
        
        base_goto_set_target(targetX, targetY, angle);
      }
      
      gripper_release();
      
      further_adjustments = true;
        
      if (pow(robotPosition[0] - targetX, 2) < 0.001 && pow(robotPosition[1] - targetY, 2) < 0.001)
      {
        // Went to the position, switch to pickup mode
        MyStatus = PickingUpBox;
      } else {
        counter = 0;
      }
      
      blocked_counter1 = 0;
    } else if (MyStatus == DeliveringBox)
    {    
      arm_set_height(ARM_RESET);
      
      targetX = selectedDropPoints[finishedDropPoints][0];
      targetY = selectedDropPoints[finishedDropPoints][1];
      
      if (reversePos)
      {
        if (robotPosition[1] > 0)
        {
          targetY = 1;
          targetX = targetX;
        } else {
          targetY = -1;
          targetX = targetX;
        }
      }
      
      double offset = 0;
      
      if (KBOT_INDEX == 0)
        offset = offset_corner;
      else 
        offset = 0.48;
      
      // Red works in horizontal, blue in vertical
      if (KBOT_INDEX == 0)
      {
        if (targetX < 0)
        {
          angle = 0;
          targetX -= offset;
        }
        else 
        {
          angle = PI;
          targetX += offset;
        }
      } else {
        if (targetY > 0)
        {
          angle = PI/2;
          targetY += offset;
        } else {
          angle = -PI/2;         
          targetY -= offset;
        }
      }
      
      if (ongoingTask == 2)
      {
        // Red must do first
        if ((signal < 2 && KBOT_INDEX == 1) || KBOT_INDEX == 0)
        {
          // Reach the cylinder thing
          if (KBOT_INDEX == 1)
          {
            targetX = 1;
            targetY = 0.39;
            angle = PI / 2;
          } 
          else if (KBOT_INDEX == 0)
          {
            targetX = 1;
            targetY = -0.39;
            angle = -PI / 2;
          }
          
          if (cylinderHeight > 5)
          {         
            // more than maximum height
            arm_set_sub_arm_rotation(ARM1, 0);
            arm_set_sub_arm_rotation(ARM2, 0);
            arm_set_sub_arm_rotation(ARM3, 0);
            arm_set_sub_arm_rotation(ARM4, -(PI / 3));
          } else {
            // reach maximum height
            arm_set_sub_arm_rotation(ARM1, 0);
            arm_set_sub_arm_rotation(ARM2, 0);
            arm_set_sub_arm_rotation(ARM3, 0);
            arm_set_sub_arm_rotation(ARM4, -PI / 2);
          }
        }
      }
      
      if (counter > time_for_grip_start + time_for_grip_end + 25)
      {
        if (blocked_counter1 % 400 == 0)
        {
          double d = sqrt(pow(previousPos[0] - robotPosition[0], 2) + pow(previousPos[1] - robotPosition[1], 2));
          
          printf("Last distance: %f\n",d);
          
          if (d < 0.001)
          {
            // officially stuck
            reversePos = true;
          } else {
            reversePos = false;
          }
          
          previousPos[0] = robotPosition[0];
          previousPos[1] = robotPosition[1];
        }         
        base_goto_set_target(targetX, targetY, angle);  
      }
      
      // Once positioned 
      if (!reversePos && pow(targetX - robotPosition[0], 2) < 0.001 && pow(targetY - robotPosition[1], 2) < 0.001)
      {
        arm_ik_counter++;
        
        if (ongoingTask == 1)
        {
          if (KBOT_INDEX == 0)
          {
            arm_ik(0.18, 0.14, 0);
          } else {
            arm_ik(0.28, 0.13, 0);
          }
          
          if ((KBOT_INDEX == 0 && arm_ik_counter > time_for_gripper_positioning) || 
              (KBOT_INDEX == 1 && arm_ik_counter > time_for_gripper_center_positioning))
          {
            gripper_release();
            
            if ((KBOT_INDEX == 0 && arm_ik_counter > time_for_gripper_positioning + 20) || 
              (KBOT_INDEX == 1 && arm_ik_counter > time_for_gripper_center_positioning + 40))
            {             
              if (KBOT_INDEX == 0)
              {
                arm_ik(0.18, 0.20, 0);
              } else {
                arm_ik(0.28, 0.15, 0);
              }
              
              if ((KBOT_INDEX == 0 && arm_ik_counter > time_for_gripper_positioning + 40) || 
              (KBOT_INDEX == 1 && arm_ik_counter > time_for_gripper_center_positioning + 100))
              {
                finishedDropPoints++;
                
                arm_set_height(ARM_RESET);
                
                // Reset the routine
                counter = 0;
                arm_ik_counter = 0;              
                MyStatus = ReachingBox;
                has_target = false;
                
                if (finishedDropPoints == 4)
                {
                  ongoingTask = 2;
                }
              }
            }
          }
       
          printf("Must put the box back\n");
        } 
        else if (ongoingTask == 2)
        {
          if (arm_ik_counter > 50)
          {
            gripper_release();
            
            if (arm_ik_counter > 200)
            {
              MyStatus = Idle;
            }
          }
        }
      } 
         
    } 
    else if (MyStatus == Idle)
    {
      base_backwards();
    }
    
    avoidCollision();
    
  } 
  
  wb_robot_cleanup();
  return 0;
}
