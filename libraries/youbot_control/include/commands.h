#include <webots/robot.h>
#include <webots/nodes.h>
#include <webots/supervisor.h>
#include <webots/camera.h>
#include <webots/range_finder.h>

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h> 

#include <arm.h>
#include <base.h>
#include <gripper.h>

#define TIME_STEP 16

char buff[255];
char *splits[2];
FILE *fp;    
int command;

char forwardCommand[10] = "forward";

void readfile()
{
   fp = fopen("../commands.txt", "r");
   fscanf(fp, "%s", buff);
}
   
void readCommands()
{
   splits[0] = strtok(buff, ",");
   splits[1] = strtok(NULL, " ,.-");
   command = atoi(splits[KBOT_INDEX]);
}

void doCommand()
{
  switch (command)
  {
    case 0:
      base_forwards();    
      break;
    case 1:
      base_backwards();
      break;
    case 2:
      base_strafe_left();
      break;
    case 3:
      base_strafe_right();
      break;
    case 4:
      base_turn_left();
      break;
    case 5:
      base_turn_right();
      break;
    case 6:
      // pickup stage
      // set height and things
      break;
  }
}

int main(int argc, char **argv) {
  // initialization: robot
  wb_robot_init();
  
  // initialization: robot base, arm and the gripper
  base_init();
  arm_init();
  gripper_init();
  
  // Main loop
  while (wb_robot_step(TIME_STEP) != -1) {
   readfile();
   readCommands();
   doCommand();
  }
  wb_robot_cleanup();
  return 0;
}
